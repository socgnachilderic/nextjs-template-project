module.exports = {
  reactStrictMode: true,
  eslint: {
    dirs: ['src']
  },
  i18n: {
    locales: ['en-UK', 'fr'],
    defaultLocale: 'en-UK'
  }
}
