tools:
    - linter: eslint
    - formatter: prettier
    - docs: storybook
    - test: jest, enzyme
    - framework: nextJS/ReactJS
    - css style: scss module
    - organization: fonctional structure