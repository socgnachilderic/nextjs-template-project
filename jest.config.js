module.exports = {
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    testEnvironment: 'jsdom',
    collectCoverageFrom: [
        'src/**/*.{ts,tsx}',
        '__tests__/**/*.{ts,tsx}',
        '!**/tests/**',
        '!**/node_modules/**',
        '!**/.storybook/**',
        '!**/coverage/**',
        '!jest.config.js',
    ],
    coverageThreshold: {
        global: {
            branches: 100,
            functions: 100,
            lines: 100,
            statements: 100,
        },
    },
    testPathIgnorePatterns: [
        '/.next/',
        '/node_modules/',
        '/lib/',
        '/tests/',
        '/coverage/',
        '/.storybook/',
    ],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    moduleNameMapper: {
        // '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js',
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/__mocks__/fileMock.js',
    },
};
