export interface UserBase {
    firstname?: string;
    lastname?: string;
    email?: string;
    phoneNumber?: string;
    currentPassword?: string
    password?: string;
    confirmPassword: string;
}

export interface UserLoginDCO extends UserBase {
    email: string;
    password: string;
}

export interface UserDCO extends UserBase {
    firstname: string;
    lastname: string;
    email: string;
    phoneNumber: string;
    password: string;
    confirmPassword: string;
}

export interface UserDTO extends UserBase {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
}
