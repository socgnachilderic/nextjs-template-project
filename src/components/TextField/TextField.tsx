import React, { FC } from 'react';
import { TextFieldContainer } from './TextField.style';

interface Props {
    type?: string;
}

/**
 * Composant d'interface utilisateur pour la récupérer la saisie de l'utilisateur
 *
 * ### Usage
 *
 * ```jsx
 *  <TextField type="text" />
 * ```
 */
export const TextField: FC<Props> = ({ type = 'text' }) => {
    return <TextFieldContainer type={type} />;
};
