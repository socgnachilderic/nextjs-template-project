import styled from 'styled-components';

export const ButtonStyle = styled.button`
    text-decoration: none;
    border: none;
    outline: none;
    padding: .625rem 1rem;
    line-height: 1.5;
    border-radius: 5px;
    background-color: crimson;
    color: white;
    margin: 5px;

    &:hover {
        cursor: pointer;
    }
`
